---
title: "About"
description: "frzndev is all about hacking"
featured_image: ''
menu:
  main:
    weight: 1
---

frzndv (pronounced Frozen Dev) is a small group of *nix hackers, software architects, hardware devs, video game enthusiasts, artists, and writers. We hang out and sometimes we make stuff. We try and teach each other stuff when we have time.